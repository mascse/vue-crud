<!DOCTYPE html>
<html>
<head>
  <title>My first Vue app</title>
  <!-- <script src="https://unpkg.com/vue"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
  <div id="app">
    {{ message }}<br>
    {{ test }}
  </div>

  <script>
    var app = new Vue({
      el: '#app',
      data: {
        message: 'Hello Vue!',
        test: 'Thanks'
      }
    })
  </script>

</body>
</html>